const { app, BrowserWindow, ipcMain, shell } = require('electron');
function createWindow() {
    const win = new BrowserWindow({
        width: 816,  //遊戲解析度寬度 (Screen Width)
        height: 624, //遊戲解析度高度 (Screen Height)
        icon: 'icon/icon.png',
        useContentSize: true,
        autoHideMenuBar: true,
        backgroundColor: '#000000',
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    })
    const isTest = false; //是否為測試模式 (Whether it is a test mode.)
    const testString = isTest ? '?test' : '';
    win.loadURL(`file://${__dirname}/index.html${testString}`);

    if (process.platform !== 'darwin') {
        win.setMenu(null);
    }
    require('electron').ipcMain.on('focusMainWindow', function (e) {
        win.focus();
    });

    require('electron').ipcMain.on('openDevTools', function (e) {
        win.webContents.openDevTools();
    });

    require('electron').ipcMain.on('openExternal', function (event, arg) {
        shell.openExternal(arg);
    });

};

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
    app.quit();
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});




